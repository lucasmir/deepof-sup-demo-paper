[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Flucasmir%2Fdeepof-sup-demo-paper/master)

<br>

<div align="center">
  <img width="350" height="350" src="https://gitlab.mpcdf.mpg.de/lucasmir/deepof/-/raw/master/logos/deepOF_logo_w_text.png">
</div>

Main data and code repository for "Automatically annotated motion tracking identifies a distinct social behavioral profile following chronic social defeat stress", including:


* Code split along two notebooks, including supervised and unsupervised analyses respectively. Both notebooks can be executed live on a binder environment, by either clicking the badge on top of this readme, or the link [here](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Flucasmir%2Fdeepof-sup-demo-paper/master).


* Representative video snippets for each cluster depicted in the unsupervised analysis, which can be found [here](https://datashare.mpcdf.mpg.de/s/O0KaFNMCM5NIkGE).


* DeepOF, the package we developed and used in all analyses, is available in [this](https://gitlab.mpcdf.mpg.de/lucasmir/deepof) repository.


* The official documentation for DeepOF can be found in [this](https://deepof.readthedocs.io/en/latest/) website.


* The original pre-print is also available [here](https://www.biorxiv.org/content/10.1101/2022.06.23.497350v1).

